<?php
$urlbase=$_POST['url'];
$cuestionario=$_POST['cuestionario'];
$nombre=$_POST['nombre'];
include_once "../conexion/conexion.php";
include_once "../pages/cabecera.php";
require_once "../support/http.php";
require_once "../support/web_browser.php";
require_once "../support/simple_html_dom.php";
// Simple HTML DOM tends to leak RAM like
// a sieve.  Declare what you will need here.
// Objects are reusable.
$html = new simple_html_dom();
$url=$urlbase."/".$cuestionario;
//$url = "http://cuestionarios.chiquipedia.com/-serie-dibujos-1.html";
$web = new WebBrowser();
$result = $web->Process($url);
if (!$result["success"])  echo "Error retrieving URL.  " . $result["error"] . "\n";
else if ($result["response"]["code"] != 200)  echo "Error retrieving URL.  Server returned:  " . $result["response"]["code"] . " " . $result["response"]["meaning"] . "\n";
else
{
    //header('Content-Type: text/html; charset=utf-8');
    echo "Web Crawling ...... :\n";
    $html->load($result["body"]);
    $rows = $html->find("table[width=500] p strong");
    $rows2 = $html->find("table[width=500] tr td div");
    //table p 'devuelve todas las preguntas
    //table[width=500] tr td div 'devuelve todas las alternativas.
    echo '<table border="1">';
    $i=0;
    foreach ($rows as $row)
    {
        $preguntas[$i] =  utf8_encode(strip_tags($row));
        $i = $i +1;
        echo '<tr>';
        echo "<td>";
        echo "\t" . $row . "\n";
        echo "</td></tr>";
    }
    $i=0;
    foreach ($rows2 as $row)
    {
        $alternativas[$i] = utf8_encode(strip_tags($row));
        $i = $i +1;
        echo '<tr>';
        echo "<td>";
        echo "\t" . $row . "\n";
        echo "</td></tr>";
    }
    echo "</table>";
    //recuperamos el último ID de los cuestionarios
    $query = "select id from questionnaire order by id desc limit 1;";
    $resultado = pg_query($link, $query) or die("Error en la Consulta SQL");
    $numReg = pg_num_rows($resultado);
    if($numReg>0){
       while ($fila=pg_fetch_array($resultado)) {
            $ncuestionarios=$fila['id'];
       }
    }
    //recuperarmos el último ID de las preguntas
    $query = "select id from question order by id desc limit 1;";
    $resultado = pg_query($link, $query) or die("Error en la Consulta SQL");
    $numReg = pg_num_rows($resultado);
    if($numReg>0){
       while ($fila=pg_fetch_array($resultado)) {
            $npreguntas=$fila['id'];
       }
    }
    //recuperamos el último ID de las alternativas
    $query = "select id from alternative order by id desc limit 1;";
    $resultado = pg_query($link, $query) or die("Error en la Consulta SQL");
    $numReg = pg_num_rows($resultado);
    if($numReg>0){
       while ($fila=pg_fetch_array($resultado)) {
            $nalternativas=$fila['id'];
       }
    }
    //echo $ncuestionarios." ".$npreguntas." ".$nalternativas;
    //empezamos a guardar empezando por el cuestionario
    $ncuestionarios=$ncuestionarios+1;
    $query = "insert into questionnaire(id,idtopic,title,type) values ($ncuestionarios,1,'$nombre',1);";
    pg_query($link, $query) or die("Error en la Consulta SQL");
    //luego empezamos a guardar las preguntas
    for($i=0;$i<10;$i++){
        $npreguntas=$npreguntas+1;
        $query = "insert into question(id,code,question,comments,state,iddifficulty,alternativecorrect,idquestionnaire) values ($npreguntas,$i+1,'$preguntas[$i]',' ',1,0,0,$ncuestionarios);";
        pg_query($link, $query) or die("Error en la Consulta SQL");
         //finalmente empezamos a guardar las aternativas
        for($j=0;$j<4;$j++){
            $nalternativas=$nalternativas+1;
            $k=$i*4+$j;
            $query = "insert into alternative(id,description,idquestion) values ($nalternativas,'$alternativas[$k]',$npreguntas);";
            pg_query($link, $query) or die("Error en la Consulta SQL");
        }
    }
    echo "<------------------------- IMPORTACION SATISFACTORIA ------------------------->";
    include_once "../pages/pie.php";
    //cerrar conexion
    //pg_connect($link);
}

?>